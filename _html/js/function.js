﻿$(function() {

  var formNames = [];
  // フォームのname属性を収集
  $('.qa_area').each(function(index, el) {
    $(el).find('input[type=radio]').each(function(index_i, el_i) {
      $(el_i).click(function(event) {
        if (checkInput()) {
          $('.btn_result').addClass('btn_result_on');
        }
      });
    });
  });
  // 初回実行
  checkInput();

  // 入力チェック
  function checkInput() {
    var chk_result = true;
    $('.qa_area').each(function(index, el) {
      if ($(el).find('input[type=radio]:checked').length == 0) {
        chk_result = false;
        return false;
      }
    });

    if (chk_result) {
      return true;
    }else{
      return false;
    }
  }


  $('.btn_result').click(function(e){
    if($('.radio01 input[type="radio"]:checked').length < 1) {
      return;
    }
    else if($('.radio02 input[type="radio"]:checked').length < 1) {
      return;
    }
    else if($('.radio03 input[type="radio"]:checked').length < 1) {
      return;
    }

    var param = "";
    if(!(window.location.search === "")) { param = window.location.search; }
    location.href = 'https://www.papawash.co.jp/lp/ai33007a/?_adp_c=wa&_adp_e=c&_adp_u=p&_adp_p_md=7766&_adp_p_cp=80019&_adp_p_agr=8476452&_adp_p_ad=9590571&token_id=${token_id}&vendor=1001' + param;
  });

});
